# Final Project - Group I03 

🔗 **APK LINK:** https://drive.google.com/drive/folders/10E7gYm8xWmf1ggGfye45OOu4zNGVdCmq?usp=sharing

# 📱 Our application idea
The COVID-19 pandemic affects not only public health, but also the economy, education, and social life of the Indonesian people. Especially younger generations who spent most of their time socializing. 


<p align="center">
<img src="https://i.imgur.com/NFvUxkw.png" alt="drawing" width="250"/>
</p>

**Mutuals** is an app that will allow end user to connect with a diverse group of people sharing the same interests. 

An innovation that we would like to develop through our app is a **built in chat feature** which will automatically appear when two end users are matched. 
There will also be a **functionality that will allow end user to shuffle a group of people with the same interest and randomly select a user to chat with.** This can be used as a starting point, as a new user starts to navigate throughout the app, especially those who lack in confidence to start a new conversation with another end user. 

Lastly, since we do not recommend them to physically meet, our application will also provide call feature to ease the communication, however this is for feature implementation due to our lack of skills 👋🏼

# 💡 Modules that we will implement
➡️ log-in or sign-up system: Arya 

➡️ Create the user interest update form: Ryan

➡️ Create the user interest form: Aldi

➡️ Create a social media button: Audi

➡️ Create the randomizer button: Kevin

➡️ Create the suggested friends based on the same interest: Felix

➡️ Create the contact feature: Joel

# 🧑🏻 User persona
<p align="center">
<img src="https://i.imgur.com/5pOnJgS.png" alt="drawing" width="700"/>
</p>

# ⏳ How the web application is integrated to the web service
1. Implement authentication cookies for login and signup user
2. Retrieve the backend data from the web service using JSONResponse or Django's JSON Serializer
3. Execute the front-end mobile app design based on the web application by using flutter widgets

# 👩🏻‍💻 Group Members: 
- Anastasia Audi - 2006607495
- Reynaldi Oktavianus - 2006607601 
- Leonard Felix Sadewa - 2006607583 
- Joel Adhika - 2006607564 
- Ryan Putra - 2006519990 
- Arya Chandra - 2006607526
- Kevin Heryanto - 2006607570
