

import 'package:flutter/material.dart';
import '../widgets/nav-drawer.dart';

import '../page/profile.dart';
import '../page/login.dart';

class signup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: const NavDrawer(),
        appBar: AppBar(
            title: const Text('Signup'),
        centerTitle: true,
        backgroundColor: const Color(0xFFFFF385),
        foregroundColor: Colors.black,),
        body: SingleChildScrollView(
            child: Column(
          children: [
            const SizedBox(height: 30),
            title(),
            const SizedBox(height: 15),
            username(),
            const SizedBox(height: 50),
            email(),
            const SizedBox(height: 50),
            password(),
            const SizedBox(height: 50),
            submit(),
            const SizedBox(height: 30),
            signin()
          ],
        )));
  }
}

class title extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        
        alignment: Alignment.center,
        child: const Center(
          child: Padding(padding: EdgeInsets.all( 25.0),
          child: Text(
            "Sign up and let's be Mutuals 🕺🏻 ",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 40),
          ),
        )
    ));
  }
}

class username extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'Username',
        ),
      ),
    );
  }
}

class email extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'Email',
        ),
      ),
    );
  }
}

class password extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        obscureText: true,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'Password',
        ),
      ),
    );
  }
}

class submit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 200,
        height: 50,
        child: ElevatedButton(
          // Respond to button press
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => profile()),
            );
          },
          style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(const Color(0xFFFEF284)),
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)))),
          child: const Text(
            "Submit",
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ));
  }
}

class signin extends StatelessWidget{
  @override 
  Widget build (BuildContext context){
    return TextButton(
          child: const Text('Already have an account', style: TextStyle(decoration: TextDecoration.underline, fontSize: 20, color: Colors.black),),
          onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => login()),
            )
          
        );
  }
}