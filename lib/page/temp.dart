import 'package:flutter/material.dart';
import 'package:mutuals/page/suggestion.dart';

import 'dart:convert';
import 'package:mutuals/page/user.dart';
import 'package:flutter/services.dart' as rootbundle;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:mutuals/page/contact.dart';

// to see rawstring of temp

class Temp extends StatefulWidget{

  @override
  State<Temp> createState() => _TempState();
}

class _TempState extends State<Temp> {

  // retrieve from assets/json/test.json
  List<User> _user = <User>[];
  Future<List<User>> fetchUser() async{
    var response = await rootbundle.rootBundle.loadString('assets/json/test.json');
    var user = <User>[];
    print(response.length);
    var userJson = json.decode(response);
    for(var eachuser in userJson){
      user.add(User.fromJson(eachuser));
    }
    return user;
  }


  // get user data from shared preferences
  List<User> _userData = [];
  String _userPrefString = "";
  void _loadUserList() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _userPrefString = prefs.getString('user') ?? "";
      if(_userPrefString.isNotEmpty){
        var userJson = json.decode(_userPrefString);
        for(var eachuser in userJson){
          _userData.add(User.fromJson(eachuser));
        }
      }
    });
  }

  @override
  void initState(){
    fetchUser().then((value){
      setState((){
        _user.addAll(value);
      });
    });
    _loadUserList(); // from shared preferences
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const Text("success"),

          Text(
            _userPrefString
          ),

          Expanded(
            child: ListView.builder(itemBuilder: (context, index){
              return Card(
                  child: Material(
                      color: const Color(0xFFFFFAC9),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Contact(selecteduser: _userData[index])));
                        },
                        highlightColor: const Color(0xFFFFFAC9),
                        child: Column(
                              children: <Widget>[
                                Text(_userData[index].Fullname.toString()
                                ),
                                Text(_userData[index].Interest.toString())
                              ],
                        ),
                      )
                  )
              );
            },
              itemCount: _userData.length,
            ),
          ),

          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Suggestion()),
              );
            },
            child: const Text("Next"),
          )
        ],
      )
    );
  }
}