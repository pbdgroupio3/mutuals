class User{
  String? Fullname;
  String? Birthdate;
  String? Domicile;
  String? Gender;
  String? Line;
  String? Instagram;
  String? Email;
  String? Interest;
  String? Username;
  String? Password;

  User(this.Fullname, this.Birthdate, this.Domicile, this.Gender, this.Line, this.Instagram, this.Email, this.Interest, this.Username, this.Password);

  User.fromJson(Map<String, dynamic> json){
    Fullname = json["fullname"];
    Birthdate = json["DOB"];
    Domicile = json["domicile"];
    Gender = json["gender"];
    Line = json["line"];
    Instagram = json["instagram"];
    Email = json["email"];
    Interest = json["interest"];
    Username = json["username"];
    Password = json["password"];
  }

  Map<String, dynamic> toJson() => {
    "fullname": Fullname,
    "DOB": Birthdate,
    "domicile": Domicile,
    "gender": Gender,
    "line": Line,
    "instagram": Instagram,
    "email": Email,
    "interest": Interest,
    "username": Username,
    "password": Password,
  };
}