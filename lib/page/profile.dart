// ignore_for_file: unnecessary_null_comparison

import 'package:flutter/material.dart';
import 'package:mutuals/page/signup.dart';
import '../widgets/nav-drawer.dart';
import '../page/interest.dart';
import 'dart:convert';

class profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: const NavDrawer(),
        appBar: AppBar(
            title: const Text("signup"),
            iconTheme: const IconThemeData(color: Colors.black, size: 30),
            titleTextStyle: const TextStyle(
                color: Colors.black, fontSize: 30, fontFamily: 'Futura'),
            backgroundColor: const Color(0xFFFEF284)),
        body: SingleChildScrollView(
            child: Column(
          children: [
            const SizedBox(height: 50),
            Title(),
            const SizedBox(height: 30),
            Fullname(),
            const SizedBox(height: 50),
            Date(),
            const SizedBox(height: 50),
            Domicile(),
            const SizedBox(height: 50),
            Lineid(),
            const SizedBox(height: 50),
            Instagram(),
            const SizedBox(height: 50),
            Phone_number(),
            const SizedBox(height: 50),
            submit(),
            const SizedBox(height: 50),
          ],
        )));
  }
}

class Title extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 130,
        alignment: Alignment.center,
        child: const Center(
          child: Text(
            "Hi, Name!",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 50),
          ),
        ));
  }
}

class Fullname extends StatelessWidget {
  late String name;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'Fullname',
        ),
      ),
    );
  }

  static fromJson(json) {}

  toJson() {}
}

class Date extends StatelessWidget {
  late String dob;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'YYYY-MM-DD',
        ),
      ),
    );
  }

  static fromJson(json) {}

  toJson() {}
}

// class date extends StatefulWidget {
//   dateState createState() => dateState();
// }

// class dateState extends State<date> {
//   late DateTime _dateTime;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//       children: <Widget>[
//         Text(_dateTime == null ? 'Please pick date of birth' : _dateTime.toString()),
//         RaisedButton(
//           child: Text('Pick a date'),
//           onPressed: (){
//             showDatePicker(
//               context: context,
//               initialDate: DateTime.now(),
//               firstDate: DateTime(1900),
//               lastDate: DateTime(3000)
//             ).then((date) {
//               setState(() {
//                _dateTime = date!;
//               });
//             });
//           },
//           )
//       ],
//     )
//     );
//   }

// }

class Domicile extends StatelessWidget {
  late String city;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'Domicile',
        ),
      ),
    );
  }

  static fromJson(json) {}

  toJson() {}
}

class Lineid extends StatelessWidget {
  late String line;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'lineid',
        ),
      ),
    );
  }

  toJson() {}
}

class Instagram extends StatelessWidget {
  late String ig;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'instagram',
        ),
      ),
    );
  }

  toJson() {}
}

class Phone_number extends StatelessWidget {
  late String number;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'phone_number',
        ),
      ),
    );
  }

  toJson() {}
}

class Email extends StatelessWidget {
  late String user_email;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: const TextField(
        style: TextStyle(height: 0),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              borderSide: BorderSide(width: 3.0)),
          labelText: 'email',
        ),
      ),
    );
  }

  toJson() {}
}

class Json {
  Fullname name;
  Date dob;
  Domicile city;
  Lineid line;
  Instagram ig;
  Phone_number number;
  Email user_email;

  Json(this.name, this.dob, this.city, this.line, this.ig, this.number,
      this.user_email);

  Map toJson() {
    Map name = this.name != null ? this.name.toJson() : null,
        dob = this.dob != null ? this.dob.toJson() : null,
        city = this.city != null ? this.city.toJson() : null,
        line = this.line != null ? this.line.toJson() : null,
        ig = this.ig != null ? this.ig.toJson() : null,
        number = this.number != null ? this.number.toJson() : null,
        userEmail = this.user_email != null ? this.user_email.toJson() : null;

    return {
      'name': name,
      'dob': dob,
      'city': city,
      'line': line,
      'ig': ig,
      'number': number,
      'user_email': userEmail
    };
  }
  // factory Json.fromJson(dynamic json) {
  //   return json(
  //       Fullname.fromJson(json['name']),
  //       Date.fromJson(json['DOB']),
  //       Domicile.fromJson(json['city']),
  //       Lineid.fromJson(json['line']),
  //       Instagram.fromJson(json['ig']),
  //       Phone_number.fromJson(json['number']),
  //       Email.fromJson(json['user_email']));
//   }
//   @override
//   String toString() {
//     return '{${this.name}, ${this.DOB}, ${this.city}, ${this.line}, ${this.ig}, ${this.number}, ${this.user_email}}';
//   }
}

class submit extends StatelessWidget {
  String data =
      '{"name": "fullname": ${Fullname()}", "DOB": "DOB :${Date()}", "city" : "domicile : ${Domicile()}", "line": "line : ${Lineid()}", "ig": "instagram : ${Instagram()}, "number" : "phone number : ${Phone_number()}, "user_email" : "email : ${Email()}}';
  Fullname name = Fullname();
  Date dob = Date();
  Domicile city = Domicile();
  Lineid line = Lineid();
  Instagram ig = Instagram();
  Phone_number number = Phone_number();
  Email user_email = Email();
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 200,
        height: 50,
        child: ElevatedButton(
          // Respond to button press
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => interest()),
            );
            String jsonname = jsonEncode(name);
            String jsondob = jsonEncode(dob);
            String jsoncity = jsonEncode(city);
            String jsonline = jsonEncode(line);
            String jsonig = jsonEncode(ig);
            String jsonnumber = jsonEncode(number);
            String jsonemail = jsonEncode(user_email);
            print(jsonname);
            print(jsondob);
            print(jsoncity);
            print(jsonline);
            print(jsonig);
            print(jsonnumber);
            print(jsonemail);
          },
          style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(const Color(0xFFFEF284)),
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)))),
          child: const Text(
            "Submit",
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ));
  }
}
