import 'package:flutter/material.dart';
import '../widgets/nav-drawer.dart';

class chatnow extends StatelessWidget {
  bool alert = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavDrawer(),
        appBar: AppBar(
          leading: IconButton(
              icon: const Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.pop(context)),
          title: Text('Mutuals'),
          centerTitle: true,
          backgroundColor: const Color(0xFFFFF385),
          foregroundColor: Colors.black,
        ),
        body: Center(child: Column(
          children: <Widget>[
            Image.asset('assets/images/logo.png', height: 200,),

            const SizedBox(height: 50),

            const Text('More features are coming soon :)',
              style: TextStyle(
                  fontSize: 20, fontFamily: 'Futura'
              ),
            ),

          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),

        )
    );
  }
}