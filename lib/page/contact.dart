// import 'dart:io';
import 'package:flutter/material.dart';
import 'package:mutuals/assets/instagram_icon.dart';
import 'package:mutuals/assets/line_icon.dart';
import 'package:mutuals/page/comingsoon.dart';
import 'package:mutuals/page/user.dart';
import 'package:mutuals/page/chatnow.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'dart:js' as js;


class Contact extends StatelessWidget{
  const Contact({Key? key, required this.selecteduser}) : super(key: key);

  final User selecteduser;
  

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
        icon: const Icon(Icons.arrow_back, color: Colors.black),
        onPressed: () => Navigator.pop(context)),
        title: const Text('Contact',style: TextStyle( fontFamily: 'Futura')),
        centerTitle: true,
        backgroundColor: const Color(0xFFFFF385),
        foregroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child: Wrap(
          children: [ 
            const SizedBox(height: 15),

            //image header
            Container(  
              width: double.infinity,
              height: 300,
              alignment: Alignment.center,
              child: Padding(
                padding:const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (selecteduser.Gender.toString() == "Female")...[
                       Image.asset('assets/images/female.png',  width: 250, height: 250)]
                    else if (selecteduser.Gender.toString() == "Male")...[
                        Image.asset('assets/images/male.png',  width: 250, height: 250)]
                  ]
                  ),
                ),
            ),

            const SizedBox(height: 15),

            chatnow(),
          
            const SizedBox(height: 30),
            
            //contact info
           Container(
            height: 400.0,
            color: Colors.transparent,
            child: Container(
              decoration: const BoxDecoration(
                color: Color(0xFFFEF284),
                borderRadius:  BorderRadius.only(
                  topLeft:  Radius.circular(40.0),
                  topRight:  Radius.circular(40.0),
                )
              ),
              
                child: Center(child:Column(children: [

                const SizedBox(height: 20),

                //NAME 
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(padding: const EdgeInsets.only(left: 25.0),
                  child: Row(children: [
                    Text(selecteduser.Fullname.toString(), style: const TextStyle(color: Colors.black, fontSize: 30, fontFamily: 'Futura')),
                    Padding(padding: const EdgeInsets.only(left: 50.0),
                    child: Text(selecteduser.Gender.toString(), style: const TextStyle(color: Colors.black, fontSize: 1, fontFamily: 'Futura'))
                    )]
                  ),
                  ), 
                ),

                // const SizedBox(height: 20),

                //GENDER | BIRTHDATE | DOMICILE
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(padding:const EdgeInsets.fromLTRB(25.0, 20.0, 25.0, 20.0),
                  child: Text.rich(
                    TextSpan(
                      children: <TextSpan>[
                        TextSpan(text:''+ selecteduser.Gender.toString(), style: const TextStyle(color: Colors.black, fontSize: 18, fontFamily: 'Futura')),
                        const TextSpan(text:'   |   ', style: TextStyle(color: Colors.black, fontSize: 18)),
                        TextSpan(text: selecteduser.Birthdate.toString(), style: const TextStyle(color: Colors.black, fontSize: 18, fontFamily: 'Futura')),
                        const TextSpan(text:'   |   ', style: TextStyle(color: Colors.black, fontSize: 18)),
                        TextSpan(text: selecteduser.Domicile.toString(), style:const TextStyle(color: Colors.black, fontSize: 18, fontFamily: 'Futura')),
                      ],
                    ),
                  )
                  ), 
                ),

                // const SizedBox(height: 30),

                //EMAIL 
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(padding: const EdgeInsets.only(left: 25.0),
                  child: Row(children: [
                    const Icon(Icons.mail_outline, size: 35,),
                    TextButton(
                      onPressed: () async{
                        final toEmail = selecteduser.Email.toString();
                        final subject = "Let's be Mutuals, I'm " + selecteduser.Fullname.toString();
                        final body = "Hi, nice to meet you! Im " + selecteduser.Fullname.toString();
                        final url = 'mailto:$toEmail?subject=${Uri.encodeFull(subject)}&body=${Uri.encodeFull(body)}';
                        
                          await launch(url);
                        
                      },
                      child: Text("   " + selecteduser.Email.toString(), style: const TextStyle(color: Colors.black, fontSize: 20, fontFamily: 'Futura'))
                    )
                  ],
                  )
                  )
                  ), 
                
                const SizedBox(height: 20),

                //INSTAGRAM 
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(padding: const EdgeInsets.only(left: 25.0),
                  child: Row(children: [
                    const Icon(MyFlutterApp.instagram, size: 35,),
                    TextButton(
                      onPressed: () async{
                        final url = 'https://www.instagram.com/' + selecteduser.Instagram.toString() + '/';
                        
                        if (await canLaunch(url)){
                          await launch(
                            url,
                            forceSafariVC: true,
                            forceWebView: true,
                            enableJavaScript: true
                          );
                        }
                        //js.context.callMethod('open', ['https://www.instagram.com/' + selecteduser.Instagram.toString() + '/']);
                      
                      }, child: MouseRegion(
                        cursor: SystemMouseCursors.click,
                        child: Text("   " + selecteduser.Instagram.toString(), style: const TextStyle(color: Colors.black, fontSize: 20,))
                      )
                    ),
                  ],
                  )
                  ),
                  ),
                   
                const SizedBox(height: 20),

                //LINE 
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(padding: const EdgeInsets.only(left: 25.0),
                  child: Row(children: [
                    const Icon(Line.line, size: 35,),
                    TextButton(
                      onPressed: () async{
                        final url = 'https://line.me/R/ti/p/' + selecteduser.Line.toString() + '/';
                        
                        if (await canLaunch(url)){
                          await launch(
                            url,
                            forceSafariVC: true,
                            forceWebView: true,
                            enableJavaScript: true
                          );
                        }                      
                      }, child: MouseRegion(
                        cursor: SystemMouseCursors.click,
                        child: Text("   " + selecteduser.Line.toString(), style: const TextStyle(color: Colors.black, fontSize: 20,))
                      )
                    ),

                  ],
                  )
                  )
                  ), 
                  ]
                  )
                  )
                  ), 
                  )

            ])
        )
        
        );
    }
    }

  class chatnow extends StatelessWidget{
    @override 
    Widget build(BuildContext context){
      return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children:[ ElevatedButton(
                // Respond to button press
                onPressed: () { 
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => comingsoon()
                    )
                  );
                },
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Color(0xFFFEF284)),),
                child: const Text( 
                  "chat now", 
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
          
      
    );
  }

}






