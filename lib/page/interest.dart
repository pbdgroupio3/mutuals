import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mutuals/page/suggestion.dart';
import '../widgets/nav-drawer.dart';
import 'dart:io';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import '../page/profile.dart';

class interest extends StatefulWidget {
  interestState createState() => interestState();
}

class interestState extends State<interest> {

  var interestList = [];

  // var jsonFile = File('');
  var jsonFile = File('assets\json\interest.json');
  Directory dir = Directory("assets\json\interest.json");
  // Directory dir;
  String fileName = "interest.json";
  bool fileExists = false;
  Map<String, String> fileContent = {};

  @override
  void initState() {
    super.initState();
    getApplicationDocumentsDirectory().then((Directory directory) {
      dir = directory;
      jsonFile = File(dir.path + "/" + fileName);
      fileExists = jsonFile.existsSync();
      if (fileExists){ 
        this.setState(() => fileContent = json.decode(jsonFile.readAsStringSync()));
      }
    });
  }

  // @override
  // void dispose (){
  //   keyInputController.dispose();
  //   valueInputController.dispose();
  //   super.dispose();
  // }

  void createFile(Map<String,List> content, Directory dir, String fileName)
  {
    File file = File(dir.path + "/" + fileName);
    // File file = File("assets");
    file.createSync();
    fileExists = true;
    file.writeAsStringSync(json.encode(content));
  }

  void writeToFile(String key , List value)
  {
    Map<String,List> content = {key:value};
    if(fileExists)
    {
      Map<String,List> jsonFileContent = json.decode(jsonFile.readAsStringSync());
      jsonFileContent.addAll(content);
      jsonFile.writeAsStringSync(json.encode(jsonFileContent));
    } else
    {
      createFile(content, dir, fileName);
    }
    this.setState(() => fileContent = json.decode(jsonFile.readAsStringSync()));
  }

  var singleCheckboxValue = false;
  var checkboxListLabel = [
    'Fantasy',
    'Romance',
    'Thrillers',
    'Mystery',
    'Comedy',
    'Documentary',
    'Comic',
    'Fashion',
    'Soccer',
    'Basketball',
    'Swimming',
    'Gaming',
    'Reading',
    'Cooking',
    'Design',
    'Music',
    'Food',
    'Art',
    'School',
    'Teaching',
    'Movies',
    'K-Dramas',
    'K-POP',
    'Technology',
    'Painting',
    'Concerts',
    'Piano',
    'Guitar',
    'Makeup',
    'Research',
    'Dancing',
    'Singing',
  ];

  Map<String, bool> checkboxListValues = {};
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      appBar: AppBar(
          title: const Text('Profile', style: TextStyle( fontFamily: 'Futura')),
        centerTitle: true,
        backgroundColor: const Color(0xFFFFF385),
        foregroundColor: Colors.black,),
      body: SingleChildScrollView(
      child :Column(
        children: [
          Text(
            'Please tick the box',
            style:  Theme.of(context).textTheme.headline4,
          ),
           Column(children: _checkboxList()),
           const SizedBox(height: 30),
            submit(),
            const SizedBox(height: 50),
        ],
      ),
    ));
  }


  List<Widget> _checkboxList() {
    return checkboxListLabel
        .map((label) => CheckboxListTile(
              title: Text(label),
              value: checkboxListValues[label] ?? false,
              onChanged: (newValue) {
                setState(() {
                  if (checkboxListValues[label] == null) {
                    checkboxListValues[label] = true;
                    interestList.add(label);
                    writeToFile('interest', interestList);
                  }
                  //checkboxListValues[label] = !checkboxListValues![label];
                });
              },
            ))
        .toList();
  }
}

class submit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 200,
        height: 50,
        child: ElevatedButton(
          // Respond to button press
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Suggestion()),
            );
          },
          style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(const Color(0xFFFEF284)),
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)))),
          child: const Text(
            "Submit",
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ));
  }
}