import 'package:flutter/material.dart';
import 'package:mutuals/page/editprofile.dart';
import '../widgets/nav-drawer-afterlogin.dart';
import 'package:mutuals/page/user.dart';
import '../page/interest.dart';
import 'package:flutter/services.dart' as rootbundle;
import 'dart:convert';
User test_user = User(
        'Anastasia Audi W',
        '25/10/02',
        'Tangerang',
        'Female',
        'anastasiaaudiw',
        'anastasiaaudiw',
        'anastasiaaudiw@gmail.com',
        "watch, sleep, eat",
        "audi",
        "test123");


class myprofile extends StatefulWidget {
  _myprofilestate createState() => _myprofilestate();
}

class _myprofilestate extends State<myprofile> {
  TextEditingController _controllerFullName = TextEditingController();
  TextEditingController _controllerDate = TextEditingController();
  TextEditingController _controllerDomicile = TextEditingController();
  TextEditingController _controllerLineId = TextEditingController();
  TextEditingController _controllerInstagram = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  List<User> _user = <User>[];

  Future<List<User>> fetchUser() async {
    var response =
        await rootbundle.rootBundle.loadString('assets/json/test.json');
    var userlist = <User>[];

    var userJson = json.decode(response);
    for (var eachuser in userJson) {
      userlist.add(User.fromJson(eachuser));
    }
    return userlist;
  }

  @override
  void initState() {
    fetchUser().then((value) {
      setState(() {
        _user.addAll(value);
      });
    });
    _controllerFullName.text = test_user.Fullname.toString();
    _controllerDate.text = test_user.Birthdate.toString();
    _controllerDomicile.text = test_user.Domicile.toString();
    _controllerEmail.text = test_user.Email.toString();
    _controllerInstagram.text = test_user.Instagram.toString();
    _controllerLineId.text = test_user.Line.toString();
    return super.initState();
  }

  @override
  Widget build(BuildContext context) {
    fetchUser().then((value) {
      setState(() {
        _user.addAll(value);
      });
    });
    return Scaffold(
        drawer: const NavDrawerAfter(),
        appBar: AppBar(
          title:
              const Text('My Profile', style: TextStyle(fontFamily: 'Futura')),
          centerTitle: true,
          backgroundColor: const Color(0xFFFFF385),
          foregroundColor: Colors.black,
        ),
        body: SingleChildScrollView(
            child: Column(
          children: [
            title(),
            Container(
              width: 250,
              child: TextField(
                readOnly: true,
                style: TextStyle(height: 0),
                decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      borderSide: BorderSide(width: 3.0)),
                  labelText: 'Fullname',
                ),
                controller: _controllerFullName,
                onChanged: (String value) {
                  test_user.Fullname = value;
                },
              ),
            ),
            const SizedBox(height: 50),
            Container(
              width: 250,
              child: TextField(
                readOnly: true,
                style: TextStyle(height: 0),
                controller: _controllerDate,
                onChanged: (String value) {
                  test_user.Birthdate = value;
                },
                decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      borderSide: BorderSide(width: 3.0)),
                  labelText: 'Date',
                ),
              ),
            ),
            const SizedBox(height: 50),
            Container(
              width: 250,
              child: TextField(
                readOnly: true,
                style: TextStyle(height: 0),
                controller: _controllerDomicile,
                onChanged: (String value) {
                  test_user.Domicile = value;
                },
                decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      borderSide: BorderSide(width: 3.0)),
                  labelText: 'Domicile',
                ),
              ),
            ),
            const SizedBox(height: 50),
            Container(
              width: 250,
              child: TextField(
                readOnly: true,
                style: TextStyle(height: 0),
                controller: _controllerInstagram,
                onChanged: (String value) {
                  test_user.Instagram = value;
                },
                decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      borderSide: BorderSide(width: 3.0)),
                  labelText: 'Instagram',
                ),
              ),
            ),
            const SizedBox(height: 50),
            Container(
              width: 250,
              child: TextField(
                readOnly: true,
                style: TextStyle(height: 0),
                controller: _controllerLineId,
                onChanged: (String value) {
                  test_user.Line = value;
                },
                decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      borderSide: BorderSide(width: 3.0)),
                  labelText: 'Line ID',
                ),
              ),
            ),
            const SizedBox(height: 50),
            Container(
              width: 250,
              child: TextField(
                readOnly: true,
                style: TextStyle(height: 0),
                controller: _controllerEmail,
                onChanged: (String value) {
                  test_user.Email = value;
                },
                decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      borderSide: BorderSide(width: 3.0)),
                  labelText: 'Email',
                ),
              ),
            ),
            const SizedBox(height: 50),
            submit()
          ],
        )));
  }
}

class title extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 130,
        alignment: Alignment.center,
        child: const Center(
          child: Text(
            "My Profile 👤",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 50, fontFamily: 'Futura'),
          ),
        ));
  }
}

class submit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 200,
        height: 50,
        child: ElevatedButton(
          // Respond to button press
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => editprofile()),
            );
          },
          style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(const Color(0xFFFEF284)),
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)))),
          child: const Text(
            "Edit Profile",
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontFamily: 'Futura'),
          ),
        ));
  }
}
