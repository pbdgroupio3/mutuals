// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:mutuals/widgets/nav-drawer-afterlogin.dart';
import '../widgets/nav-drawer-afterlogin.dart';

class aboutAfter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawerAfter(),
      appBar: AppBar(
        title: const Text('About'),
        centerTitle: true,
        backgroundColor: const Color(0xFFFFF385),
        foregroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child: Column(
        children: [ const
          SizedBox(height: 50),
          title(),
          description(),
          member1(),
          member2(),
          member3(),
          member4()
          ],
      )
      )
    );
  }
}
class title extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 130,
      alignment: Alignment.center,
      child:const Center(
        child: 
            Text(
              'Behind the Scenes 🚪',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 50, fontFamily: 'Futura'),
            ),
        )
      );
  }
}

  class description extends StatelessWidget{
    @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      alignment: Alignment.center,
      child:const Center(
        child: Text(
              'Meet the people behind the project.',
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 20, fontFamily: 'Futura'),
            )
      )
    );
  }
}
class member1 extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 250,
      alignment: Alignment.center,
      child: Center(
        child:Image.asset(
            'assets/images/member1.png',  
            width: 350,
            height: 350)
      )
    );
  }
}

class member2 extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 250,
      alignment: Alignment.center,
      child: Center(
        child:Image.asset(
            'assets/images/member2.png',  
            width: 350,
            height: 350)
      )
    );
  }
}
class member3 extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 250,
      alignment: Alignment.center,
      child: Center(
        child:Image.asset(
            'assets/images/member3.png',  
            width: 350,
            height: 350)
      )
    );
  }
}

class member4 extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 200,
      alignment: Alignment.center,
      child: Center(
        child:Image.asset(
            'assets/images/member4.png',  
            width: 350,
            height: 350)
      )
    );
  }
}
