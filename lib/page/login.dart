import 'package:flutter/material.dart';
import '../widgets/nav-drawer.dart';
import '../page/suggestion.dart';

import 'dart:io';
import 'dart:convert';
import 'package:mutuals/page/user.dart';
import 'package:flutter/services.dart' as rootbundle;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path_provider/path_provider.dart';

class login extends StatefulWidget {
  _login createState() => _login();
}

class _login extends State<login> {

  // retrieve from assets/json/test.json
  List<User> _user = <User>[];
  Future<List<User>> fetchUser() async{
    var response = await rootbundle.rootBundle.loadString('assets/json/test.json');
    var user = <User>[];
    print(response.length);
    var userJson = json.decode(response);
    for(var eachuser in userJson){
      user.add(User.fromJson(eachuser));
    }
    return user;
  }

  void _setUserList(userList) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('user', jsonEncode(userList));
  }

  // get user data from shared preferences
  List<User> _userData = [];
  String _userPrefString = "";
  void _loadUserList() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      for(var i in _user){
        _userData.add(i);
      }
      _userPrefString = prefs.getString('user') ?? "";
      if(_userPrefString.isEmpty){
        _userData = _user;
        _setUserList(_user);
      } else {
        var userJson = json.decode(_userPrefString);
        for(var eachuser in userJson){
          _userData.add(User.fromJson(eachuser));
        }
      }
    });
  }

  // set current login in shared preferences
  void _setCurrentLogin(username) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('currentLogin', username);
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }
  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/test.json');
  }
  Future<File> writeUserFile(userList) async {
    final file = await _localFile;
    return file.writeAsString(jsonEncode(userList));
  }

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose(){
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  void initState(){
    fetchUser().then((value){
      setState((){
        _user.addAll(value);
      });
    });
    _loadUserList(); // from shared preferences
    super.initState();
  }

  bool isLoginValid(){
    for(var x in _userData){
      if ((x.Username == usernameController.text) &&
          (x.Password == passwordController.text)){
        return true;
      }
    }
    return false;
  }
  bool toggleError = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer:const NavDrawer(),
        appBar: AppBar(
          title: const Text('Login'),
          centerTitle: true,
          backgroundColor: const Color(0xFFFFF385),
          foregroundColor: Colors.black,
        ),
        body: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(height: 100),

                // Title
                Container(
                    width: double.infinity,
                    height: 130,
                    alignment: Alignment.center,
                    child:const Center(
                      child:
                      Text(
                        "Sign in and let's be Mutuals 🕺🏻 ",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 50),
                      ),
                    )
                ),

                SizedBox(height: 50),

                // Username
                Container(
                  width: 250,
                  child: TextField(
                    controller: usernameController,
                    style: const TextStyle(height: 0),
                    decoration: const InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(
                              width: 3.0)),
                      labelText: 'Username',
                    ),
                  ),
                ),

                SizedBox(height: 50),

                // Password
                Container(
                  width: 250,
                  child: TextField(
                    controller: passwordController,
                    style: const TextStyle(height: 0),
                    obscureText: true,
                    decoration: const InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(
                              width: 3.0)),
                      labelText: 'Password',
                    ),
                  ),
                ),

                SizedBox(height: 50),

                // Error message
                Visibility(
                    visible: toggleError,
                    child: const Text(
                      "Invalid username/password",
                      style: TextStyle(color: Colors.red),
                    )
                ),

                SizedBox(height: 50),

                // Submit button
                Container(
                    width: 200,
                    height: 50,
                    child:
                    ElevatedButton(
                      // Respond to button press
                      onPressed: () {
                        if (isLoginValid()){
                          _setCurrentLogin(usernameController.text);
                          writeUserFile(_userData);
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Suggestion()),
                          );
                        } else {
                          setState(() {
                            toggleError = true;
                          });
                        }
                      },
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(const Color(0xFFFEF284)),
                          shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)))
                      ),
                      child:const Text(
                        "Submit",
                        style: TextStyle(color: Colors.black, fontSize: 20),
                      ),
                    )
                ),

                SizedBox(height: 50),

                TextButton(
                  child: const Text(
                    'Already have an account',
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 20,
                        color: Colors.black),
                  ),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => login()),
                  )
                )

              ],
            )
        )
    );
  }
}