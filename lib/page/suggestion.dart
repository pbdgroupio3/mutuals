import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mutuals/page/contact.dart';
import 'package:mutuals/page/user.dart';
import '../widgets/nav-drawer-after.dart';
import '../page/comingsoon.dart';
import '../page/contact.dart';
import 'package:flutter/services.dart' as rootbundle;
import 'package:http/http.dart' as http;

class Suggestion extends StatefulWidget{

  Suggestionstate createState() => Suggestionstate();

}
class Suggestionstate extends State<Suggestion> {

  List<User> _user = <User>[];

  Future<List<User>> fetchUser() async{

    //-------------------------------------------------------------------\\ WARNING: ONLY USE ONE OF THEM

    // var url = 'https://mutuals-io3.herokuapp.com/json';
    // var response = await http.get(Uri.parse(url));
    // var userlist = <User>[];
    //
    // if (response.statusCode == 200){
    //   var userJson = json.decode(response.body);
    //   for(var eachuser in userJson){
    //     userlist.add(User.fromJson(eachuser['fields']));
    //   }
    // }



    //using web html
    //---------------------------------------------------------------------\\
    //using local file



     var response = await rootbundle.rootBundle.loadString('assets/json/test.json');
     var userlist = <User>[];

     var userJson = json.decode(response);
     for(var eachuser in userJson){
        userlist.add(User.fromJson(eachuser));
     }

    //----------------------------------------------------------------------\\ WARNING: ONLY USE ONE OF THEM

    return userlist;
  }

  @override
  void initState(){
    fetchUser().then((value){
      setState((){
        _user.addAll(value);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawerAfter(),
      appBar: AppBar(
        
        title: const Text('Mutuals', style: TextStyle( fontFamily: 'Futura')),
        centerTitle: true,
        backgroundColor: const Color(0xFFFFF385),
        foregroundColor: Colors.black,
      ),
      body: Center(child:
      Column(
        children: <Widget>[
          
          Padding(
            padding:const EdgeInsets.all(10.0),
            child: Image.asset('assets/images/header_sugg.png', height: 240)),

          Expanded(
              child: ListView.builder(itemBuilder: (context, index){
                return Padding(
                   padding: const EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                   
                child: Card(
                  child: Material(
                    color: const Color(0xFFFFF6A5),
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Contact(selecteduser: _user[index])));
                      },
                      highlightColor: const Color(0xFFFFF6A5),
                      child: Padding(
                          padding: const EdgeInsets.only(top: 20, bottom: 20),
                        child: Column(
                          children: <Widget>[
                            Text(_user[index].Fullname.toString(),style: const TextStyle(
                                fontSize: 30, fontFamily: 'Futura'),
                              ),
    
                            Text(_user[index].Interest.toString(), style: const TextStyle(fontFamily: 'Futura'),)
                          ],
                        )
                     ),
                    )
                  )
                )
                );
              },
              itemCount: _user.length,
              ),
          ),
          const SizedBox(height: 10),

          const Text("Still not finding who you're looking for ? \n",
            style: TextStyle(
              fontSize: 20,
            ),
            textAlign: TextAlign.center,
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Contact(selecteduser: (_user.toList()..shuffle()).first)),
              );
            },
            child: const Text("Shuffle Mutuals", style: TextStyle(fontFamily: 'Futura')),
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                ),
                primary: const Color(0xFFFFF385),
                onPrimary: Colors.black,
                minimumSize: const Size(200, 50)
            ),
          ),
          const SizedBox(height: 30),
        ],

      )
      ),

    );
  }
}