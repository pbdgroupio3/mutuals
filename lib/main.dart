import 'package:flutter/material.dart';
import './widgets/nav-drawer.dart';
import './page/signup.dart';
import './page/suggestion.dart';


void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  //the build method describes how to build the widget
  Widget build (BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mutuals',
      theme: ThemeData(fontFamily: 'Futura'),
      home: MyHomePage(),
      );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer:const NavDrawer(),
      appBar: AppBar(
        title: const Text('Mutuals'),
        centerTitle: true,
        backgroundColor: const Color(0xFFFFF385),
        foregroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child:Column(
        children: [ const
          SizedBox(height: 50),
          logo(),
          title(),
          description(),
          create_account(),
          ],
      ))
    );
  }
}

class  logo extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(  
        width: double.infinity,
        height: 200,
        alignment: Alignment.center,
        child: Padding(
          padding:const EdgeInsets.all(10.0),
          child:Image.asset(
            'assets/images/logo.png',  
            width: 300,
            height: 300)
        )
    );
  }
}

class title extends StatelessWidget{
  @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 130,
      alignment: Alignment.center,
      child: const Center(
        child: 
            Text(
              'Welcome to Mutuals 👋🏼',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 50),
            ),
        )
      );
  }
}

class description extends StatelessWidget{
    @override 
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 130,
      alignment: Alignment.center,
      child: const Center(
        child: Text(
              'Mutuals is an application that \n focuses in connecting end user to \n a diverse group of people sharing \n the same interests.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20),
            )
      )
    );
  }
}

class create_account extends StatelessWidget{
  @override 
  Widget build(BuildContext context){
    return Container(
      height: 150,

      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[const
            Text(
              "Haven't created an account?",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              // Respond to button press
              onPressed: () { 
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => signup(),
                  )
                );
              },
              style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Color(0xFFFEF284)),),
              child: const Text( 
                "Let's be Mutuals", 
                style: TextStyle(color: Colors.black, fontSize: 20),
              ),
            )
          ]
        )
      )
    );
  }

}

