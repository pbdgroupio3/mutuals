import 'package:flutter/widgets.dart';

class Line {
  Line._();

  static const _kFontFam = 'Line';
  static const String? _kFontPkg = null;

  static const IconData line = IconData(0xf3c0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}