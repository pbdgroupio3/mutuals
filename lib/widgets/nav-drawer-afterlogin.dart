// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:mutuals/page/suggestion.dart';
import '../page/aboutafter.dart';
import '../page/myprofile.dart';

class NavDrawerAfter extends StatelessWidget {
  const NavDrawerAfter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createHeader(),
          const SizedBox(
            height: 16,
          ),
          buildMenuItem(
            text: 'Home',
            onClicked: () => selectedItem(context, 0),
          ),
          buildMenuItem(
            text: 'About',
            onClicked: () => selectedItem(context, 1),
          ),
          buildMenuItem(
            text: 'My Profile',
            onClicked: () => selectedItem(context, 2),
          )
        ],
      ),
    );
  }

  Widget createHeader() {
    return DrawerHeader(
        padding: EdgeInsets.zero,
        decoration: const BoxDecoration(
            color: Color(0xFFFEF284),
            image: DecorationImage(
                scale: 2.5,
                image: AssetImage(
                  'assets/images/logo.png',
                ))),
        child: Stack(children: const <Widget>[
          Positioned(
              bottom: 12.0,
              left: 16.0,
              child: Text("Side bar Menu",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500))),
        ]));
  }

  Widget buildMenuItem({required String text, VoidCallback? onClicked}) {
    return ListTile(
      title: Text(
        text,
        style: const TextStyle(fontSize: 25),
      ),
      onTap: onClicked,
    );
  }

  //index starts with 0
  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Suggestion()));
        break;
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => aboutAfter(),
        ));
        break;
      case 2:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => myprofile(),
        ));
        break;
    }
  }
}
