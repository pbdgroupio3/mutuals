// ignore_for_file: file_names

import 'package:flutter/material.dart';
import '../main.dart';
import '../page/about.dart';
import '../page/login.dart';
import '../page/signup.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createHeader(),
          const SizedBox(
            height: 16,
          ),
          buildMenuItem(
            text: 'Home',
            onClicked: () => selectedItem(context, 0),
          ),
          buildMenuItem(
            text: 'About',
            onClicked: () => selectedItem(context, 1),
          ),
          buildMenuItem(
            text: 'Login',
            onClicked: () => selectedItem(context, 2),
          ),
          buildMenuItem(
            text: 'Register',
            onClicked: () => selectedItem(context, 3),
          ),
        ],
      ),
    );
  }

  Widget createHeader() {
    return DrawerHeader(
        padding: EdgeInsets.zero,
        decoration: const BoxDecoration(
            color: Color(0xFFFEF284),
            image: DecorationImage(
                scale: 2.5,
                image: AssetImage(
                  'assets/images/logo.png',
                ))),
        child: Stack(children: const <Widget>[
          Positioned(
              bottom: 12.0,
              left: 16.0,
              child: Text("Side bar Menu",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500))),
        ]));
  }

  Widget buildMenuItem({required String text, VoidCallback? onClicked}) {
    return ListTile(
      title: Text(
        text,
        style: const TextStyle(fontSize: 25),
      ),
      onTap: onClicked,
    );
  }

  //index starts with 0
  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => MyApp()));
        break;
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => about(),
        ));
        break;
      case 2:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => login(),
        ));
        break;
      case 3:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => signup(),
        )
      );
    }
  }
}
