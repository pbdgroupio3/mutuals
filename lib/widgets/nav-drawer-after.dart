// ignore_for_file: file_names

import 'package:shared_preferences/shared_preferences.dart';
import '../main.dart';
import 'package:flutter/material.dart';
import 'package:mutuals/page/suggestion.dart';
import '../page/aboutafter.dart';
import '../page/editprofile.dart';

class NavDrawerAfter extends StatefulWidget {
  const NavDrawerAfter({Key? key}) : super(key: key);

  @override
  State<NavDrawerAfter> createState() => _NavDrawerAfterState();
}

class _NavDrawerAfterState extends State<NavDrawerAfter> {
  String currentLogin = "";

  void setCurrentUser() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      currentLogin = (prefs.getString('currentLogin')) ?? "";
    });
  }

  void _deleteCurrentUser() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('currentLogin', "");
  }

  @override
  void initState() {
    setCurrentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createHeader(),
          const SizedBox(
            height: 16,
          ),
          buildMenuItem(
            text: 'Home',
            onClicked: () => selectedItem(context, 0),
          ),
          buildMenuItem(
            text: 'About',
            onClicked: () => selectedItem(context, 1),
          ),
          buildMenuItem(
            text: 'Edit Profile',
            onClicked: () => selectedItem(context, 2),
          ),
          buildMenuItem(
            text: 'Logout',
            onClicked: () => selectedItem(context, 3),
          ),
        ],
      ),
    );
  }

  Widget createHeader() {
    return DrawerHeader(
        padding: EdgeInsets.zero,
        decoration: const BoxDecoration(
            color: Color(0xFFFEF284),
            image: DecorationImage(
                scale: 2.5,
                image: AssetImage(
                  'assets/images/logo.png',
                ))),
        child: Stack(children: const <Widget>[
          Positioned(
              bottom: 12.0,
              left: 16.0,
              child: Text("Side bar Menu",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500))),
        ]));
  }

  Widget buildMenuItem({required String text, VoidCallback? onClicked}) {
    return ListTile(
      title: Text(
        text,
        style: const TextStyle(fontSize: 25),
      ),
      onTap: onClicked,
    );
  }

  //index starts with 0
  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Suggestion()));
        break;
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => aboutAfter(),
        ));
        break;
      case 2:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => editprofile(),
        ));
        break;
      case 3:
        _deleteCurrentUser();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => MyApp()));
        break;
    }
  }
}
